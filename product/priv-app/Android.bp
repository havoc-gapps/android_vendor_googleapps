// Copyright (C) 2022 Benzo Rom
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

android_app_import {
	name: "AndroidAutoStubPrebuilt",
	privileged: true,
	apk: "AndroidAutoStubPrebuilt/AndroidAutoStubPrebuilt.apk",
	presigned: true,
	dex_preopt: {
		enabled: false,
	},
	product_specific: true,
}

android_app_import {
    name: "CarrierMetrics",
    privileged: true,
    apk: "CarrierMetrics/CarrierMetrics.apk",
    presigned: true,
    dex_preopt: {
        enabled: false,
    },
    product_specific: true,
}

android_app_import {
    name: "ConfigUpdater",
    privileged: true,
    apk: "ConfigUpdater/ConfigUpdater.apk",
    presigned: true,
    dex_preopt: {
        enabled: false,
    },
    product_specific: true,
}

android_app_import {
	name: "FilesPrebuilt",
	privileged: true,
	apk: "FilesPrebuilt/FilesPrebuilt.apk",
	presigned: true,
	dex_preopt: {
		enabled: false,
	},
	product_specific: true,
}

android_app_import {
    name: "GoogleCamera",
    overrides: ["Camera2", "Aperture"],
    privileged: true,
    apk: "GoogleCamera/GoogleCamera.apk",
    presigned: true,
	dex_preopt: {
		enabled: false,
	},
    product_specific: true,
}

android_app_import {
    name: "GoogleDialer",
    overrides: ["Dialer"],
    privileged: true,
    apk: "GoogleDialer/GoogleDialer.apk",
    presigned: true,
	dex_preopt: {
		enabled: false,
	},
    product_specific: true,
}

android_app_import {
    name: "GoogleOneTimeInitializer",
    overrides: ["OneTimeInitializer"],
    privileged: true,
    apk: "GoogleOneTimeInitializer/GoogleOneTimeInitializer.apk",
    presigned: true,
    dex_preopt: {
        enabled: false,
    },
    product_specific: true,
}

android_app_import {
    name: "GoogleRestorePrebuilt",
    privileged: true,
    apk: "GoogleRestorePrebuilt/GoogleRestorePrebuilt.apk",
    presigned: true,
	dex_preopt: {
		enabled: false,
	},
    product_specific: true,
}

android_app_import {
	name: "HealthConnectPrebuilt",
	privileged: true,
	apk: "HealthConnectPrebuilt/HealthConnectPrebuilt.apk",
	presigned: true,
	dex_preopt: {
		enabled: false,
	},
	product_specific: true,
}

android_app_import {
    name: "MaestroPrebuilt",
    privileged: true,
    apk: "MaestroPrebuilt/MaestroPrebuilt.apk",
    presigned: true,
	dex_preopt: {
		enabled: false,
	},
    product_specific: true,
}

android_app_import {
    name: "OdadPrebuilt",
    privileged: true,
    apk: "OdadPrebuilt/OdadPrebuilt.apk",
    presigned: true,
    dex_preopt: {
        enabled: false,
    },
    product_specific: true,
}

android_app_import {
    name: "PartnerSetupPrebuilt",
    privileged: true,
    apk: "PartnerSetupPrebuilt/PartnerSetupPrebuilt.apk",
    presigned: true,
    dex_preopt: {
        enabled: false,
    },
    product_specific: true,
}

android_app_import {
    name: "Phonesky",
    privileged: true,
    apk: "Phonesky/Phonesky.apk",
    presigned: true,
	dex_preopt: {
		enabled: false,
	},
    product_specific: true,
}

android_app_import {
    name: "PixelLiveWallpaperPrebuilt",
    privileged: true,
    apk: "PixelLiveWallpaperPrebuilt/PixelLiveWallpaperPrebuilt.apk",
    presigned: true,
	dex_preopt: {
		enabled: false,
	},
    product_specific: true,
}

android_app_import {
    name: "PrebuiltBugle",
    overrides: ["messaging"],
    privileged: true,
    apk: "PrebuiltBugle/PrebuiltBugle.apk",
    presigned: true,
	dex_preopt: {
		enabled: false,
	},
    product_specific: true,
}

android_app_import {
    name: "PrebuiltGmsCore",
    overrides: [
        "NetworkRecommendation",
        "SoundRecorder",
    ],
    required: [
        "AndroidPlatformServices",
        "MlkitBarcodeUIPrebuilt",
        "VisionBarcodePrebuilt",
    ],
    privileged: true,
    apk: "PrebuiltGmsCore/PrebuiltGmsCoreSc.apk",
    filename: "PrebuiltGmsCoreSc.apk",
    presigned: true,
	dex_preopt: {
		enabled: false,
	},
    product_specific: true,
}

android_app_import {
    name: "RecorderPrebuilt",
    overrides: ["Recorder"],
    privileged: true,
    apk: "RecorderPrebuilt/RecorderPrebuilt.apk",
    presigned: true,
	dex_preopt: {
		enabled: false,
	},
    product_specific: true,
}

android_app_import {
    name: "SafetyHubPrebuilt",
    overrides: ["EmergencyInfo"],
    privileged: true,
    apk: "SafetyHubPrebuilt/SafetyHubPrebuilt.apk",
    presigned: true,
	dex_preopt: {
		enabled: false,
	},
    product_specific: true,
}

android_app_import {
    name: "ScribePrebuilt",
    privileged: true,
    apk: "ScribePrebuilt/ScribePrebuilt.apk",
    presigned: true,
	dex_preopt: {
		enabled: false,
	},
    product_specific: true,
}

android_app_import {
    name: "SecurityHubPrebuilt",
    privileged: true,
    apk: "SecurityHubPrebuilt/SecurityHubPrebuilt.apk",
    presigned: true,
	dex_preopt: {
		enabled: false,
	},
    product_specific: true,
}

android_app_import {
    name: "SettingsIntelligenceGooglePrebuilt",
    overrides: ["SettingsIntelligence"],
    privileged: true,
    apk: "SettingsIntelligenceGooglePrebuilt/SettingsIntelligenceGooglePrebuilt.apk",
    presigned: true,
	dex_preopt: {
		enabled: false,
	},
    product_specific: true,
}

android_app_import {
    name: "SetupWizardPrebuilt",
    overrides: ["Provision"],
    privileged: true,
    apk: "SetupWizardPrebuilt/SetupWizardPrebuilt.apk",
    presigned: true,
	dex_preopt: {
		enabled: false,
	},
    product_specific: true,
}

android_app_import {
    name: "TurboPrebuilt",
    privileged: true,
    apk: "TurboPrebuilt/TurboPrebuilt.apk",
    presigned: true,
	dex_preopt: {
		enabled: false,
	},
    product_specific: true,
}

android_app_import {
    name: "Velvet",
    overrides: ["QuickSearchBox"],
    privileged: true,
    apk: "Velvet/Velvet.apk",
    presigned: true,
	dex_preopt: {
		enabled: false,
	},
    product_specific: true,
}

android_app_import {
    name: "WallpaperEffect",
    privileged: true,
    apk: "WallpaperEffect/WallpaperEffect.apk",
    presigned: true,
	dex_preopt: {
		enabled: false,
	},
    product_specific: true,
}

android_app_import {
    name: "WellbeingPrebuilt",
    privileged: true,
    apk: "WellbeingPrebuilt/WellbeingPrebuilt.apk",
    presigned: true,
	dex_preopt: {
		enabled: false,
	},
    product_specific: true,
}
