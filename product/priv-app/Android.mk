# Copyright (C) 2022 Benzo Rom
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := PrebuiltGmsCoreSc_AdsDynamite
LOCAL_MODULE_STEM := PrebuiltGmsCoreSc_AdsDynamite.apk
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := PrebuiltGmsCore/app_chimera/m/PrebuiltGmsCoreSc_AdsDynamite.apk
LOCAL_MODULE_PATH := $(TARGET_OUT_PRODUCT)/priv-app/PrebuiltGmsCore/app_chimera/m/
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_CLASS := APPS
LOCAL_PRIVILEGED_MODULE := true
LOCAL_NO_STANDARD_LIBRARIES := true
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_PRODUCT_MODULE := true
LOCAL_DEX_PREOPT := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := PrebuiltGmsCoreSc_CronetDynamite
LOCAL_MODULE_STEM := PrebuiltGmsCoreSc_CronetDynamite.apk
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := PrebuiltGmsCore/app_chimera/m/PrebuiltGmsCoreSc_CronetDynamite.apk
LOCAL_MODULE_PATH := $(TARGET_OUT_PRODUCT)/priv-app/PrebuiltGmsCore/app_chimera/m/
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_CLASS := APPS
LOCAL_PRIVILEGED_MODULE := true
LOCAL_NO_STANDARD_LIBRARIES := true
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_PRODUCT_MODULE := true
LOCAL_DEX_PREOPT := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := PrebuiltGmsCoreSc_DynamiteLoader
LOCAL_MODULE_STEM := PrebuiltGmsCoreSc_DynamiteLoader.apk
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := PrebuiltGmsCore/app_chimera/m/PrebuiltGmsCoreSc_DynamiteLoader.apk
LOCAL_MODULE_PATH := $(TARGET_OUT_PRODUCT)/priv-app/PrebuiltGmsCore/app_chimera/m/
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_CLASS := APPS
LOCAL_PRIVILEGED_MODULE := true
LOCAL_NO_STANDARD_LIBRARIES := true
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_PRODUCT_MODULE := true
LOCAL_DEX_PREOPT := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := PrebuiltGmsCoreSc_DynamiteModulesA
LOCAL_MODULE_STEM := PrebuiltGmsCoreSc_DynamiteModulesA.apk
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := PrebuiltGmsCore/app_chimera/m/PrebuiltGmsCoreSc_DynamiteModulesA.apk
LOCAL_MODULE_PATH := $(TARGET_OUT_PRODUCT)/priv-app/PrebuiltGmsCore/app_chimera/m/
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_CLASS := APPS
LOCAL_PRIVILEGED_MODULE := true
LOCAL_NO_STANDARD_LIBRARIES := true
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_PRODUCT_MODULE := true
LOCAL_DEX_PREOPT := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := PrebuiltGmsCoreSc_DynamiteModulesC
LOCAL_MODULE_STEM := PrebuiltGmsCoreSc_DynamiteModulesC.apk
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := PrebuiltGmsCore/app_chimera/m/PrebuiltGmsCoreSc_DynamiteModulesC.apk
LOCAL_MODULE_PATH := $(TARGET_OUT_PRODUCT)/priv-app/PrebuiltGmsCore/app_chimera/m/
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_CLASS := APPS
LOCAL_PRIVILEGED_MODULE := true
LOCAL_NO_STANDARD_LIBRARIES := true
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_PRODUCT_MODULE := true
LOCAL_DEX_PREOPT := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := PrebuiltGmsCoreSc_GoogleCertificates
LOCAL_MODULE_STEM := PrebuiltGmsCoreSc_GoogleCertificates.apk
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := PrebuiltGmsCore/app_chimera/m/PrebuiltGmsCoreSc_GoogleCertificates.apk
LOCAL_MODULE_PATH := $(TARGET_OUT_PRODUCT)/priv-app/PrebuiltGmsCore/app_chimera/m/
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_CLASS := APPS
LOCAL_PRIVILEGED_MODULE := true
LOCAL_NO_STANDARD_LIBRARIES := true
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_PRODUCT_MODULE := true
LOCAL_DEX_PREOPT := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := PrebuiltGmsCoreSc_MapsDynamite
LOCAL_MODULE_STEM := PrebuiltGmsCoreSc_MapsDynamite.apk
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := PrebuiltGmsCore/app_chimera/m/PrebuiltGmsCoreSc_MapsDynamite.apk
LOCAL_MODULE_PATH := $(TARGET_OUT_PRODUCT)/priv-app/PrebuiltGmsCore/app_chimera/m/
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_CLASS := APPS
LOCAL_PRIVILEGED_MODULE := true
LOCAL_NO_STANDARD_LIBRARIES := true
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_PRODUCT_MODULE := true
LOCAL_DEX_PREOPT := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := PrebuiltGmsCoreSc_MeasurementDynamite
LOCAL_MODULE_STEM := PrebuiltGmsCoreSc_MeasurementDynamite.apk
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := PrebuiltGmsCore/app_chimera/m/PrebuiltGmsCoreSc_MeasurementDynamite.apk
LOCAL_MODULE_PATH := $(TARGET_OUT_PRODUCT)/priv-app/PrebuiltGmsCore/app_chimera/m/
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_CLASS := APPS
LOCAL_PRIVILEGED_MODULE := true
LOCAL_NO_STANDARD_LIBRARIES := true
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_PRODUCT_MODULE := true
LOCAL_DEX_PREOPT := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := AndroidPlatformServices
LOCAL_MODULE_STEM := AndroidPlatformServices.apk
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := PrebuiltGmsCore/m/independent/AndroidPlatformServices.apk
LOCAL_MODULE_PATH := $(TARGET_OUT_PRODUCT)/priv-app/PrebuiltGmsCore/m/independent/
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_CLASS := APPS
LOCAL_PRIVILEGED_MODULE := true
LOCAL_NO_STANDARD_LIBRARIES := true
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_PRODUCT_MODULE := true
LOCAL_DEX_PREOPT := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := MlkitBarcodeUIPrebuilt
LOCAL_MODULE_STEM := MlkitBarcodeUIPrebuilt.apk
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := PrebuiltGmsCore/m/optional/MlkitBarcodeUIPrebuilt.apk
LOCAL_MODULE_PATH := $(TARGET_OUT_PRODUCT)/priv-app/PrebuiltGmsCore/m/optional/
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_CLASS := APPS
LOCAL_PRIVILEGED_MODULE := true
LOCAL_NO_STANDARD_LIBRARIES := true
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_PRODUCT_MODULE := true
LOCAL_DEX_PREOPT := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := VisionBarcodePrebuilt
LOCAL_MODULE_STEM := VisionBarcodePrebuilt.apk
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := PrebuiltGmsCore/m/optional/VisionBarcodePrebuilt.apk
LOCAL_MODULE_PATH := $(TARGET_OUT_PRODUCT)/priv-app/PrebuiltGmsCore/m/optional/
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_CLASS := APPS
LOCAL_PRIVILEGED_MODULE := true
LOCAL_NO_STANDARD_LIBRARIES := true
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_PRODUCT_MODULE := true
LOCAL_DEX_PREOPT := false
include $(BUILD_PREBUILT)